// simpletest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"



// define a macro for __declspec(dllexport) keyword instead of .def file
#define DllImport   __declspec( dllimport )

extern "C" DllImport int simpledriver( const double *x, double *y );

int _tmain(int argc, _TCHAR* argv[])
{
	const double x[] = {2.5};
	double y[] = {0.0};
	simpledriver(x, y);
	fprintf(stdout, "data is %4.2f", x[0]);
	fprintf(stdout, "out is %4.2f", y[0]);
	return 0;
}

