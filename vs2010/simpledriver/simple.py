import ctypes
import os
DIRNAME = os.path.dirname(__file__)
BUILD = os.path.join('..', 'x64', 'Release')
simpleDLL = ctypes.cdll.LoadLibrary(os.path.join(DIRNAME, BUILD, 'simpledriver.dll'))
simpledriver = simpleDLL[1]
# assumes return type is int
x = (ctypes.c_double * 1)(2.5)
y = (ctypes.c_double * 1)()
simpledriver(x, y)
print y[0]
