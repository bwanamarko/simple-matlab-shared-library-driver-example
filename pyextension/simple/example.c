#include "Python.h"
#include <math.h>
#include <string.h>
#include <stdio.h>
#include "simple.h"

static PyObject *SpamError;

static int MCR_state = 0;

static PyObject * example_init_MCR(PyObject *self) {
    mclmcrInitialize(); // set up mcr environment without initialization
    if (mclIsMCRInitialized()) {
        PyErr_SetString(SpamError, "MCR can only be initialized once.");
        return NULL;
    }
    /* Call the mclInitializeApplication routine. Make sure that the application
     * was initialized properly by checking the return status. This initialization
     * has to be done before calling any MATLAB API's or MATLAB Compiler generated
     * shared library functions.  */
    if (!mclInitializeApplication(NULL,0)) {
        PyErr_SetString(SpamError, "Could not initialize the application.");
        return NULL;
    }
    /* Call the library intialization routine and make sure that the
     * library was initialized properly. */
    if (!simpleInitialize()) {
        PyErr_SetString(SpamError, "Could not initialize the library.");
        return NULL;
    }
    printf("Initialized MATLAB simple application.\n");
    MCR_state = 1; // set MCR state to "open"
    Py_RETURN_NONE;
}

static PyObject * example_MCR_status(PyObject *self) {
    if (MCR_state > 0) {
        printf("MCR was initialized.\n");
        return Py_BuildValue("s", "open");
    } else if (MCR_state < 0) {
        printf("MCR was terminated.\n");
        return Py_BuildValue("s", "closed");
    } else {
        printf("MCR not initialized.\n");
        Py_RETURN_NONE;
    }
}

static PyObject * example_MCR_initialized(PyObject *self) {
    mclmcrInitialize(); // set up mcr environment without initialization
    if (mclIsMCRInitialized()) {
        printf("MCR has been initialized.\n");
        Py_RETURN_TRUE;
    } else {
        printf("MCR has not been initialized.\n");
        Py_RETURN_FALSE;
    }
}
    
static PyObject * example_kill_MCR(PyObject *self) {
    if (MCR_state < 0) {
        PyErr_SetString(SpamError, "MCR can only be terminated once.");
        return NULL;
}
    mclmcrInitialize(); // set up mcr environment without initialization
    if (mclIsMCRInitialized()) {
        printf("MCR Initialized!\n");
    } else {
        PyErr_SetString(SpamError, "MCR NOT Initialized!");
        return NULL;
    }
    simpleTerminate();
    mclTerminateApplication();
    MCR_state = -1; // set MCR state to "closed"
    Py_RETURN_NONE;
}

static PyObject * example_simple(PyObject *self, PyObject *args) {
    mxArray *xin; /* Define input parameters */
    mxArray *yout = NULL; /* and output parameters to be passed to the library functions */
    const double *y;
    const double x;
    
    if (!PyArg_ParseTuple(args, "d", &x))
        return NULL;

    printf("x: %f\n", x);
    
    // raise exception if MCR has been terminated
    if (MCR_state < 0) {
        PyErr_SetString(SpamError, "MCR has been terminated.");
        return NULL;
    }

    mclmcrInitialize(); // set up mcr environment without initialization
    if (mclIsMCRInitialized()) {
        printf("MCR Initialized!\n");
    } else {
        PyErr_SetString(SpamError, "MCR NOT Initialized!");
        return NULL;
    }
    
    /* get pointer to the real data in the input matrix */
    xin = mxCreateDoubleMatrix(1, 1, mxREAL);
    memcpy(mxGetPr(xin), &x, 1*sizeof(double));
    
    /* Call the library intialization routine and make sure that the
     * library was initialized properly. */
    if (!simpleInitialize()) {
        fprintf(stderr,"Could not initialize the library:\n%s.",
                mclGetLastErrorMessage());
        PyErr_SetString(SpamError, mclGetLastErrorMessage());
        return NULL;
    } else {
        /* Call the library function */
        mlfSimple(1, &yout, xin);
        y = mxGetPr(yout);
        printf("y: %f\n", *y);
        printf("address of y: %p\n", y);
        printf("x: %f\n", x);
        printf("address of xin: %p\n", xin);
        printf("address of yout: %p\n", yout);
        
        /* Destroy the return value since this variable will be reused in
         * the next function call. Since we are going to reuse the variable,
         * we have to set it to NULL. Refer to MATLAB Compiler documentation
         * for more information on this. */
        mxDestroyArray(yout); yout=0;
        /* Call the library termination routine */
        
        /* Free the memory created */
        mxDestroyArray(xin); xin=0;
        printf("y: %f\n", *y);
        printf("address of y: %p\n", y);
        printf("x: %f\n", x);
    }
    return Py_BuildValue("d", *y);
}

static PyMethodDef example_methods[] = {
    {"init_MCR", (PyCFunction)example_init_MCR, METH_NOARGS, "init_MCR() start matlab"},
    {"MCR_status", (PyCFunction)example_MCR_status, METH_NOARGS, "MCR_status() matlab status"},
    {"MCR_initialized", (PyCFunction)example_MCR_initialized, METH_NOARGS, "MCR_status() matlab status"},
    {"kill_MCR", (PyCFunction)example_kill_MCR, METH_NOARGS, "kill_MCR() stop matlab"},
    {"simple", example_simple, METH_VARARGS, "simple() run matlab"},
    {NULL, NULL}
};

PyMODINIT_FUNC initexample(void) {
    PyObject *m;
    m = Py_InitModule3("example", example_methods, "simple MATLAB MCR example");
    if (m == NULL)
        return;
    SpamError = PyErr_NewException("spam.error", NULL, NULL);
    Py_INCREF(SpamError);
    PyModule_AddObject(m, "error", SpamError);
}
