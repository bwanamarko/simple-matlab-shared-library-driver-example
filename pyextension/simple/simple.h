/*
 * MATLAB Compiler: 5.0 (R2013b)
 * Date: Mon May 19 23:51:09 2014
 * Arguments: "-B" "macro_default" "-v" "-l" "simple.m" 
 */

#ifndef __simple_h
#define __simple_h 1

#if defined(__cplusplus) && !defined(mclmcrrt_h) && defined(__linux__)
#  pragma implementation "mclmcrrt.h"
#endif
#include "mclmcrrt.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__SUNPRO_CC)
/* Solaris shared libraries use __global, rather than mapfiles
 * to define the API exported from a shared library. __global is
 * only necessary when building the library -- files including
 * this header file to use the library do not need the __global
 * declaration; hence the EXPORTING_<library> logic.
 */

#ifdef EXPORTING_simple
#define PUBLIC_simple_C_API __global
#else
#define PUBLIC_simple_C_API /* No import statement needed. */
#endif

#define LIB_simple_C_API PUBLIC_simple_C_API

#elif defined(_HPUX_SOURCE)

#ifdef EXPORTING_simple
#define PUBLIC_simple_C_API __declspec(dllexport)
#else
#define PUBLIC_simple_C_API __declspec(dllimport)
#endif

#define LIB_simple_C_API PUBLIC_simple_C_API


#else

#define LIB_simple_C_API

#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_simple_C_API 
#define LIB_simple_C_API /* No special import/export declaration */
#endif

extern LIB_simple_C_API 
bool MW_CALL_CONV simpleInitializeWithHandlers(
       mclOutputHandlerFcn error_handler, 
       mclOutputHandlerFcn print_handler);

extern LIB_simple_C_API 
bool MW_CALL_CONV simpleInitialize(void);

extern LIB_simple_C_API 
void MW_CALL_CONV simpleTerminate(void);



extern LIB_simple_C_API 
void MW_CALL_CONV simplePrintStackTrace(void);

extern LIB_simple_C_API 
bool MW_CALL_CONV mlxSimple(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);



extern LIB_simple_C_API bool MW_CALL_CONV mlfSimple(int nargout, mxArray** y, mxArray* x);

#ifdef __cplusplus
}
#endif
#endif
