Steps
=====

1. Build the wrapper and libraries in MATLAB
--------------------------------------------

::

    >> mcc -v -l simple.m

This generates:

- simple.c
- simple.def
- simple.dll
- simple.exp
- simple.exports
- simple.h
- simple.lib

NOTES:

- sometimes MATLAB throws an error if the .dll already exists

2. Compile the driver function
------------------------------

- Open the MS Windows SDK v7.0 CMD shell
- Set the environment::

    > setenv /x86 /release

- Run make::

    > nmake

This generates:

- build/simpledriver.dll
- build/simpledriver.exp
- build/simpledriver.lib
- build/simpledriver.obj
