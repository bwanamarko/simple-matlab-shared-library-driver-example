# This is an example of a distutils 'setup' script for the example_nt
# sample.  This provides a simpler way of building your extension
# and means you can avoid keeping MSVC solution files etc in source-control.
# It also means it should magically build with all compilers supported by
# python.

# USAGE: you probably want 'setup.py install' - but execute 'setup.py --help'
# for all the details.

# NOTE: This is *not* a sample for distutils - it is just the smallest
# script that can build this.  See distutils docs for more info.

from distutils.core import setup, Extension

# simple_mod = Extension('example',
#                        include_dirs = ['C:/Program Files (x86)/MATLAB/R2014a/extern/include'],
#                        library_dirs = ['C:/Program Files (x86)/MATLAB/R2014a/extern/lib/win32/microsoft'],
#                        libraries = ['mclmcrrt'],
#                        extra_link_args = ['/IMPLIB:".\Debug/simple_d.lib"', '/DEBUG /PDB:".\Debug/example_d.pdb"'],
#                        sources = ['example.c', 'simple.h'])

simple_mod = Extension('example',
                       include_dirs = ['C:/Program Files/MATLAB/R2013b/extern/include',
                                       'C:\\PROGRA~1\\MATLAB\\R2013b\\extern\\include\\win64',
                                       'simple'],
                       library_dirs = ['C:/Program Files/MATLAB/R2013b/extern/lib/win64/microsoft',
                                       'simple'],
                       libraries = ['mclmcrrt', 'simple'],
                       sources = ['simple/example.c'])

setup(name = "simple",
    version = "1.0",
    description = "A sample extension module",
    ext_modules = [simple_mod],
    data_files = [('lib/site-packages', ['simple/simple.dll'])]
)
