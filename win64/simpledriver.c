#include <math.h>
#include <string.h>
#include <stdio.h>

// include simple header
// contains documentation, function and structure prototypes, enumerations and
// other definitions and macros
#include "simple.h"

// define a macro for __declspec(dllexport) keyword instead of .def file
#define DllExport   __declspec( dllexport )

DllExport int simpledriver( const double *x , double *y)
{
    mxArray *xin; /* Define input parameters */
    mxArray *yout = NULL; /* and output parameters to be passed to the library functions */
    const double *data;
    
    /* Call the mclInitializeApplication routine. Make sure that the application
     * was initialized properly by checking the return status. This initialization
     * has to be done before calling any MATLAB API's or MATLAB Compiler generated
     * shared library functions.  */
    if( !mclInitializeApplication(NULL,0) )
    {
        fprintf(stderr, "Could not initialize the application.\n");
        return -1;
    }
    
    /* get pointer to the real data in the input matrix */
    xin = mxCreateDoubleMatrix(1, 1, mxREAL);
    memcpy(mxGetPr(xin), x, 1*sizeof(double));
    
    /* Call the library intialization routine and make sure that the
     * library was initialized properly. */
    if (!simpleInitialize()){
        fprintf(stderr,"Could not initialize the library.\n");
        return -2;
    }
    else
    {
        /* Call the library function */
        mlfSimple(1, &yout, xin);
        data = mxGetPr(yout);
		memcpy(y, data, 1*sizeof(double));
        //memcpy(y, mxGetPr(yout), 1*sizeof(double)); // access violation!
        
        /* Destroy the return value since this variable will be reused in
         * the next function call. Since we are going to reuse the variable,
         * we have to set it to NULL. Refer to MATLAB Compiler documentation
         * for more information on this. */
        mxDestroyArray(yout); yout=0; // deletes the value y points to!
        
        /* Call the library termination routine */
        simpleTerminate();
        
        /* Free the memory created */
        mxDestroyArray(xin); xin=0;
    }
    
    /* Note that you should call mclTerminate application at the end of
     * your application.
     */
    mclTerminateApplication();
    return 0;
}
