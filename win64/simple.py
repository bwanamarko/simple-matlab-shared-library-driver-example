import ctypes
import os
DIRNAME = os.path.dirname(__file__)
simpleDLL = ctypes.cdll.LoadLibrary(os.path.join(DIRNAME, 'build', 'simpledriver.dll'))
simpledriver = simpleDLL.simpledriver
# assumes return type is int
x = (ctypes.c_double * 1)(2.5)
y = (ctypes.c_double * 1)()
simpledriver(x, y)
print y[0]
